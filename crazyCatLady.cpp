///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date    01/13/2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {

	std::cout << "Crazy Cat Lady!\nOooooh! " << argv[1] << " you're so cute!" << std::endl ;
   
   return 0;
}
